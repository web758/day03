<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    .wrapper {
        width: 400px;
        text-align: left;
        border: 2px solid blue;
        padding: 50px 36px;
        align-items: center;
        margin: auto;
    }

    form {
        width: 100%;
    }

    .form-group {
        width: 100%;
        display: flex;
        margin-bottom: 16px;

    }

    .form-group-1 {
        margin-bottom: 16px;
    }

    .form-group .form-label {
        width: 100px;
        margin-right: 16px;
        background-color: #5B9BD5;
        border: 1px solid #41719C;
        padding: 10px;
        color: white;
    }

    .form-group .form-input {
        border: 1px solid #41719C;
        width: 260px;
        color: white;
    }

    .check-box {
        align-self: center;
    }

    .btn {
        text-align: center;
    }

    button {
        background-color: #70AD47;
        border: 1px solid #41719C;
        width: 150px;
        padding: 12px 10px;
        text-align: -webkit-center;
        margin-top: 20px;
        border-radius: 8px;
        color: white;
    }

    select {
        padding: 0px 20px;
        border: 1px solid #41719C;
    }
    </style>
</head>

<body>
    <div class="wrapper">
        <form action="" method="post">
            <div class="form-group form-group-1">
                <div class="form-label">Họ và tên</div>
                <input class="form-input" type="text" name="username" id="">
            </div>
            <div class="form-group">
                <div class="form-label">Giới tính</div>
                <?php
                    $gender = array("0" => "Nam", "1" => "Nữ");
                    for ($i = 0; $i < count($gender); $i++) {
                        echo '  <div class="check-box">
                            <input type="radio"  name="gender" value=" ' . $i . '">
                            <label for="html">' . $gender[$i] . '</label>
                        </div> ';
                    }
                    ?>
            </div>
            <div class="form-group form-group-1">
                <div class="form-label">Phân khoa</div>
                <select name="khoa" id="khoa">
                    <?php
                        $falcuty = array("SPA" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");
                        foreach ($falcuty as $key => $value) {
                            echo '<option value="' . $key . '">' . $value . '</option>';
                        }
                        ?>
                </select>
            </div>
            <div class="btn">
                <button class="form-button">Đăng ký</button>
            </div>
        </form>
    </div>
</body>

</html>